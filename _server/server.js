var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const fs = require('fs');
const path = require('path');
const iconv = require('iconv-lite');


app.use('/assets', express.static('./_views/assets'));
app.use('/songs', express.static('./Songs'));

app.get('/lines', (req, res) => {
  res.sendFile(path.resolve('./_views/lines.html'));
});
app.get('/blocks', (req, res) => {
  res.sendFile(path.resolve('./_views/blocks.html'));
});
app.get('/', (req, res) => {
  res.sendFile(path.resolve('./_views/admin.html'));
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});

var active_line = '';
var active_block = '';


io.on('connection', (socket) => {
  console.log('a user connected');
  socket.on('line', line => {
      active_line = line;
      io.emit('line', line);
  })
  socket.on('block', block => {
      active_block = block;
      io.emit('block', block);
  })

  socket.on('get_active_line', () => {
      socket.emit('line', active_line);
  })
  socket.on('get_active_block', () => {
      socket.emit('block', active_block);
  })

  socket.on('get_song_list', () => {
    // iso to utf-8
    var iso_files = fs.readdirSync('./Songs-iso/');
    iso_files.forEach(iso_file => {
        let iso_file_ext = path.extname('./Songs-iso/' + iso_file);

        if(iso_file_ext != '.txt' && iso_file_ext != '.sng')
            return;


        let iso_file_content = fs.readFileSync('./Songs-iso/' + iso_file);
        let iso_file_str = iconv.decode(iso_file_content, 'ISO-8859-1');
        let iso_file_buf = iconv.encode(iso_file_str, 'utf-8');
        fs.writeFileSync('./Songs/' + iso_file, iso_file_buf);
        fs.unlinkSync('./Songs-iso/' + iso_file)
    })

    // get songs in songs folder
    fs.readdir('./Songs/', (err, files) => {
        if(!err){
            files = files.filter(file => {
                return path.extname(file).toLowerCase() == '.txt' || path.extname(file).toLowerCase() == '.sng';
            });
            socket.emit('song_list', files);
        }
    });
  });
});
